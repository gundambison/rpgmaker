# Rpgmaker

The following is a list of custom plugins. Everything is included in the folder that represents the RPGMaker engine

## Getting started

Open the folder (RPGMaker MV). Download the code inside include with the manual

 

## Installation
Follow instructions inside the folder

## Support
For custom plugins, please give contributions to original plugins owner

## Roadmap
Make own plugins

## Plugins list

- RPGMaker 2003. Planned
- RPGMaker VX Ace

1. Random Item using Analyze Tools. Using common event.

- RPGMaker MV 

1. Translation Engine (Robert Borghese (SumRndmDde))
2. SRPG_core (Ohisama Craft). This is planned

## Todo list 2023

1. learning Java and finish Abdul Kadir book
2. making manual XAS ABS (RPGMaker VX Ace) on Steam
3. get a decent paying job
4. started making Justice League Snyder Verse 2 using simple toys and backgrounds
