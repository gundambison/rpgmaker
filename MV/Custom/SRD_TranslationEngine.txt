source: http://sumrndm.site/translation-engine/

This plugin provides game developers with tools necessary to easily 
and effectively translate and localize all the text within their game. 
This is through both an in-game tool that can be used while playtesting 
and a powerful notetag system for translating database inputs.

Template to use for All Common fields

<Spanish Translation>
[name]:Haroldo
[nickname]:Harldy
[profile]:Este es el héroe del juego.
Le gusta caminar por la playa y jugar juegos.
</Spanish Translation>

added by Gundambison
<Spanish Translation>
|||name
Haroldo
|||nickname
Harldy
|||profile
Este es el héroe del juego.
Le gusta caminar por la playa y jugar juegos.
</Spanish Translation>

how to properly install the plugin:

1) Place this plugin at the bottom of your plugin list!

2) Fill out the [Languages] plugin parameter with all the languages the 
   game will be translated to.

3) Set the [Source Language Name] plugin parameter to the name of the 
   language used by the text by default.

4) Finally, set [Provide Option?] ON or OFF based on whether or not you 
   wish to provide the player with the option to change the language in 
   the options menu.