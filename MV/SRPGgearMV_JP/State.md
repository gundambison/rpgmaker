<link rel="stylesheet" href="./mystyle.css">
<a href='./Readme.md'>Return to Readme</a>
<h2>State or Type</h2>

Flow state on this SRPG :
<ol>
<li>Start</li>
<li>Player. Have sub state
<ul>
<li>Before Battle</li>
<li>After Action</li>
</ul>
</li>
<li>Enemy. Have sub state
<ul>
<li>Before Battle</li>
<li>After Action</li>
</ul>
</li>
<li>End Turn. Return to Player</li>
</ol>
<div>All information below translate from guide inside the game</div>
<h3>Start</h3>
Processing at the start of battle

Notes: <b>&lt;type:battleStart&gt;</b>

Trigger: OK button

Executes only once when SRPG_Battle Start is performed. If there are multiple pages, the top page will be executed.

In addition to the initialization process on the plugin side,
Enter the event you want to run.

Example: Production at the start of battle
Conversation event at the start of battle, etc.

Type on script

<b>$gameSystem.clearSrpgWinLoseCondition();</b>

<b>$gameSystem.setSrpgWinCondition("Do something");</b>

<b>$gameSystem.setSrpgLoseCondition("Allies defeat");</b>

<b>this.mapSelfSwitchesControl('D', 'off');</b>

suggestion: added plugins: 
<b>DisableSRPGPrepare</b>

<img src='./tutorial/start01.jpg' />

<h3>Your Player Turn</h3>
Processing at the beginning of an actor's turn

Note: <b>&lt;type:actorTurn&gt;</b>

Trigger: OK button

Executed at the start of an actor's turn. If there are multiple pages, 
the top page will be executed.

It is used for production and events at the start of the actor turn.

Example: Message display/BGM change
Conversation event at the start of the turn, etc.

<h3>Enemy Turn</h3>
Processing at the start of enemy turn

Note: <b>&lt;type:enemyTurn&gt;</b>

Trigger: OK button

Executed at the start of the enemy's turn. If there are multiple pages, 
the top page will be executed.

Used for effects and events at the start of the enemy turn.

Example: Message display/BGM change
Conversation event at the start of the turn, etc.

<h3>End Turn</h3>
Action at end of turn

Note: <b>&lt;type:turnEnd&gt;</b>

Trigger: OK button

Executed after all actions of actors and enemies.
If there are multiple pages, the top page will be executed.

Because it is executed after adding the number of turns, The number of 
elapsed turns is shifted by 1.
Example: If you want to execute an event after 5 turns have passed

　　→ Execute with 6 elapsed turns

Enter the event you want to execute at the end of your turn.

Example: Production and conversation event at the end of the turn
Battle victory/defeat after a certain number of turns have passed

The number of turns is stored in a variable (default ID 3).

<img src='./tutorial/endTurn01.jpg' />
 
Use condition based on variable 3, example view on stage 3 on event end turn

<img src='./tutorial/endTurn02.jpg' />

<h3>Before Battle</h3>
pre-battle

Name: <b>&lt;type:beforeBattle&gt;</b>

Trigger: OK button

Executed before a unit moves and begins combat.
If there are multiple pages, the top page will be executed.

Conversation events of actors and enemies
This is an expected event.

<h3>After Action</h3>

Unit action post-processing

Name: <b>&lt;type:afterAction&gt;</b>

Trigger: OK button

Executed when the unit has finished its action.
If there are multiple pages, the top page will be executed.

After each unit action of the actor/enemy
You can enter an event to be executed. 
Also, as an important function, Check win/lose condition use to do

Certain units are incapacitated or
The number of existing actors and enemies is 0, etc.
If the conditions are met,

<b>Plugin Command: SRPGBattle End</b>

You can end the battle by executing this


<h3>Conclusion</h3>
For better stage, you need to add all event-state. Even you don't need all state, this is good template to follow for your map.

<a href='./Readme.md'>Return to Readme</a>