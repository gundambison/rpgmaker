<link rel="stylesheet" href="./mystyle.css">
<a href='./Readme.md'>Return to Readme</a>
<h2>Tutorial</h2>
There is 2 kind tutorial, internal tutorial and external. Internal tutorial related to use SRPG for event inside the battle. THe external tutorial topic related to added new plugins or features

<table class='tblShow'>
<thead>
<tr><th>Name</th><th>Detail</th><th>Status</th></tr>
</thead>
<tbody>
<tr>
<td>Move Around</td>
<td>You are give a character. Move into right destination to finish the stage </td>
<td>Planning</td>
</tr>
<tr>
<td>Basic Fight</td>
<td>You are given a character. Defeat the enemy to win the stage </td>
<td>Planning</td>
</tr>
<tr>
<td>Basic Fight v2</td>
<td>You are given 2 character (random). Defeat the enemy to win the stage </td>
<td>Planning</td>
</tr>
<tr>
<td>Basic Fight v3</td>
<td>You are given 3 character (random). You will fight powerfull enemy, when defeat it there are sentence before return to save point </td>
<td>Planning</td>
</tr>
<tr>
<td>Basic Fight with AI </td>
<td>You are given 3 character (random) with AI. You will fight a lot enemy, defeat if no character alive </td>
<td>Planning</td>
</tr>
<tr>
<td>Basic Fight with AI v2</td>
<td>You are given 3 character (random) with AI. You will fight a lot enemy, defeat if no character alive or your AI defeat </td>
<td>Planning</td>
</tr>
<tr>
<td>3 turn to win </td>
<td>You are given 3 character (random). try to stay alive on 3 turn to win</td>
<td>Planning</td>
</tr>
<tr>
<td>3 turn to worse!! </td>
<td>You are given 3 character (random). try to stay alive on 3 turn to win. And reinforcement show up</td>
<td>Planning</td>
</tr>
<tr>
<td>Staying Alive vs Summon </td>
<td>You are given 3 character (random). try to stay alive on 3 turn to win. Your Enemy Summon enemy to you</td>
<td>Planning</td>
</tr>
<tr>
<td>Enemy Gain buff </td>
<td>You are given 3 character (random). Leveling your character before enter the stage. In the start battle, your enemy gain buff</td>
<td>Planning, needed added increase HP?</td>
</tr>
<tr>
<td>Aoe Showcase </td>
<td>You are given 1 character, leveling your character and learn new spell. Your enemy will not move to you and you cant approach him closely</td>
<td>Planning</td>
</tr>
<tr>
<td>Aoe Showcase v2 </td>
<td>You are given 1 character. Your enemy will not move to you and you can approach him closely. There is a bomb on ground.. Try stand near it to receive damage</td>
<td>Planning need animation and script to damage</td>
</tr>
<tr>
<td>Enemy Got Poison </td>
<td>You are given 3 character. Your enemy will decrease HP during time. Try not defeat them without weapon (let poison do the job)</td>
<td>Planning need animation and script to damage all enemy. </td>
</tr>
<tr>
<td>Hero Got Poison </td>
<td>You are given 3 character (random). Your character will decrease HP during time. Try defeat your enemy before poison defeat you</td>
<td>Planning need animation and script to damage all hero</td>
</tr>
<tr>
<td>Special Power Break </td>
<td>You are given 1 + 3 character (random) + AI. Your character will gain more power if AI defeat. Try ignore saving AI</td>
<td>Planning need animation and script event. Based on how Gohan gain Saiyan 2 power</td>
</tr>
<tr>
<td>Take Item</td>
<td>You are given  3 character (random) + NPC. Your character must take item to win the stage</td>
<td>simple tutorial like main stage 1</td>
</tr>
<tr>
<td>Take Item v2</td>
<td>You are given  3 character (random) + NPC. Your character must take all item to win the stage</td>
<td>simple tutorial like main stage 1</td>
</tr>
<tr>
<td>Boss fight</td>
<td>You are given  3 character (random). Your character must defeat bos of the stage, to learn he gain more power and form when defeat</td>
<td>simple tutorial like main stage 3</td>
</tr>
<tr>
<td>Translate</td>
<td>Using translate plugins, we can have 2 languages on the game</td>
<td>Not simple, because to many variable to add</td>
</tr>
<tr>
<td>Icon on Menu</td>
<td>Menu on battle, have icon in front of the word</td>
<td>Not simple, because to many variable to add</td>
</tr>
<tr>
<td>Lure Enemy</td>
<td>You are given  3 character (random) + NPC. NPC will move to destination area, if NPC enter the area, you win</td>
<td>Not simple, because enemy will attract to NPC or AI.</td>
</tr>
<tr>
<td>Gain New Form</td>
<td>You are given  1 character. If you enter a crystal, you will gain new form</td>
<td>Change class when touch crystal</td>
</tr>
<tr>
<td>Talk no Jutsu</td>
<td>You are given  5 character (Random) + AI. You can win after 5 turn. During battle, AI will talking a lot and stop talking after 5 turn and you win </td>
<td>it seem talking no jutsu is powerfull attack</td>
</tr>
<tr>
<td>Fly</td>
<td>You are given  1 character can fly + 5 character (Random), choose the fly character too. 
The fly can enter all area easyly. The enemy also have ability to fly</td>
<td>it seem conquer the Air is essential</td>
</tr>
<tr>
<td>Elemental Matter</td>
<td>You are given  5 character (Random), choose anti-elemen to enemy. The enemy will receive more damage</td>
<td>it seem conquer the Air is essential</td>
</tr>
<tr>
<td>Fly Enemy use Range</td>
<td>You are given  5 character (Random), choose elemen attack or range attack to enemy. Mele attack not working</td>
<td>change/added plugins</td>
</tr>
<tr>
<td>Fix menu</td>
<td>You are given new menu looks</td>
<td>need added more plugins</td>
</tr>
<tr>
<td>Fix menu</td>
<td>You are given new menu looks</td>
<td>need added more plugins
<br/>
</td>
</tr>
</tbody>
</table>

<a href='./Readme.md'>Return to Readme</a>