//-----------------------------------------------------------------------------
// SRPG_Custom.js
// Copyright (c) 2020 SRPG Team. All rights reserved.
// Released under the MIT license.
// http://opensource.org/licenses/mit-license.php
//=============================================================================

/*:
 * @plugindesc SRPG Custom functions.
 * @author Gundambison
 *
 *
 * @param varIdMP:int
 * @text Var id MP
 * @desc Set variable id for MP recovery.
 * @type number
 * @default 7
 *
 * @help
 * === Commands to Execute Custom Function ===
 * SRPGStartNoMP
 * Your player and Enemy will start having 0 MP
 *
 * SRPGRecoverAll
 * Your player will recover (HP & MP)
 * 
 * SRPGGainMP
 * added var id MP and execute to recover all mp on map
 */


function SRPG_Custom() {
  throw new Error('This is a static class');
}
(function() {
	//var parameters = PluginManager.parameters('SRPG_Custom');
	SRPG_Custom.params = null;
	SRPG_Custom.frames =   getParam("frames", 90);
	SRPG_Custom.varIdMP =  getParam("varIdMP", 7);
	
function getParam(paramName, defaultValue){
	if( SRPG_Custom.params === null){
		var params = {};
		let timeStart = new Date( ).getTime() / 1000;;
		let configParams = PluginManager.parameters('SRPG_Custom');

		for (key in configParams) {
			value = configParams[key];
			clearKey = parseKey(key);
			typeKey =  parseKey(key, 1);//type
			params[clearKey] = valueClean = parseParamItem(typeKey, value);

		}
		let timeNow = new Date( ).getTime() / 1000;
		let diffTime = timeNow - timeStart;
		console.log("compile:" + diffTime);
		
		SRPG_Custom.params = params;
	}else{
		params = SRPG_Custom.params;
	}
//-----------------
	if (params[paramName] === undefined) {
		console.log('not found:'+paramName);
		return defaultValue;
	}
	return	params[paramName];
}

function parseKey(keyRaw, positionKey=0) {
  return keyRaw.split(":")[positionKey];
}

function parseParamItem(paramType, paramValue){
	//added later
	switch (paramType) {
		case "i":
		case "int":
		case "number":
			return Number(paramValue);
		case "s":
		case "string":
		case "c":
		case "char":
			return String(paramValue);
		default:
			return paramValue;
	}
	return paramValue;
}

	
	Game_System.prototype.isPrepareEnabled = function() {
        if (this._enablePrepare === undefined) this._enablePrepare = true;
        return this._enablePrepare;
    };
	
	var _Game_Interpreter_pluginCommand = Game_Interpreter.prototype.pluginCommand;
	Game_Interpreter.prototype.pluginCommand = function(command, args) {
        _Game_Interpreter_pluginCommand.call(this, command, args);
        if (command === 'SRPGStartNoMP') $gameSystem.SRPG_startMP0( ); 
        if (command === 'SRPGRecoverAll') $gameSystem.SRPG_recoverAll( ); 
        if (command === 'SRPGGainMP') $gameSystem.SRPG_gainMP( ); 
		console.log( command );
    };
	
	 Game_System.prototype.SRPG_gainMP = function() {
		 
	//only enemies and AI
		$gameMap.events().forEach(function(event) {
		  var eventId=event.eventId();
		  var battlerArray = $gameSystem.EventToUnit(eventId);
		//----------------
		  if (battlerArray && (battlerArray[0] === 'actor' || battlerArray[0] === 'enemy')) {
			var mpUnit =  $gameVariables.value( SRPG_Custom.varIdMP );
			if (battlerArray[1].isAlive()) battlerArray[1].gainMp(  mpUnit );
		  }
		//----------------
		});
		
	 };
	 Game_System.prototype.SRPG_recoverAll = function() {
		console.log($dataSystem );
		$dataSystem.partyMembers.forEach(function(actorId) {
			var actor = $gameActors.actor( actorId);
			if (actor) {
				actor.recoverAll();
				//console.log("recover all battler");console.log(actor, actorId );
			}

		});
		
	 } 
	 
	 Game_System.prototype.SRPG_startMP0 = function() {
		//recoverAll friend
		this.SRPG_recoverAll();
		$dataSystem.partyMembers.forEach(function(actorId) {
			var actor = $gameActors.actor(actorId);
			if (actor) {
				actor.gainMp( -actor.mmp );
				//console.log("decrease mp");console.log(actor,  actorId);
			}
		});
	//only enemies and AI
		$gameMap.events().forEach(function(event) {
		  var eventId=event.eventId();
		  var battlerArray = $gameSystem.EventToUnit(eventId);
		//----------------
		  if (battlerArray && (battlerArray[0] === 'actor' || battlerArray[0] === 'enemy')) {
			var mpUnit = battlerArray[1].mmp;//max mp
			if (battlerArray[1].isAlive()) battlerArray[1].gainMp( -mpUnit);
		  }
		//----------------
		});
		
	} 
	
})();