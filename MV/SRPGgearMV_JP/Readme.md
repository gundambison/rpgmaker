<link rel="stylesheet" href="./mystyle.css">
Resource inside the game not include here

Sample Game (JP), Download and Github :

https://ohisamacraft.nyanta.jp/SrpgMaterial.html

Github :

https://github.com/Ohisama-Craft/SRPG_GearMV

Please take other SRPG here :

https://shoukanghong.github.io/Shoukang_SRPG_plugin/

To run SRPG mode properly, you need to give state, or in this plugins called as type

<a href='./State.md'>State (Click here to continue)</a>. State is something like mode on your game. 
On this game there is few mode:<ul>
<li>battle Start</li>
<li>actor Turn</li>
<li>enemy Turn</li>
<li>turn End</li>
<li>before Battle</li>
<li>After Action</li>
</ul>

<h3>Tutorial</h3>

To understand the plugins, you can view the tutorial stage. This is the tutorial list:
<ul>
<li>Simple Battle</li>
<li>Conditional to win Battle</li>
<li>Conditional to lose Battle</li>
<li>Added feature</li>
<li>And more</li>
</ul>
more list (onprogress) <a href='./Tutorial.md'>Stage list(Click here to continue)</a>.